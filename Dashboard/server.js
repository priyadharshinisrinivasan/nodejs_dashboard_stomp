'use strict';
var http = require('http');
var port = process.env.PORT || 8087;
var express = require('express');
var app = new express();
var path = require('path');
var WebSocketServer = require('ws').Server;
var moment = require('moment');
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var server = http.createServer(app).listen(8088, '0.0.0.0');

var wss = new WebSocketServer({

    server: server,
    path: '/client',
    perMessageDeflate: false,
    port: 8087
});



wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        console.log('received: %s', message);
    });   
});

wss.broadcast = function broadcast(msg) {
    
    wss.clients.forEach(function each(client) {
        client.send(msg);
    });
};


app.use('/client', express.static(path.join(__dirname + '/public')));
app.use('/zone2', express.static(path.join(__dirname + '/zone2')));
app.use('/zone3', express.static(path.join(__dirname + '/zone3')));
app.use('/zone4', express.static(path.join(__dirname + '/zone4')));


app.post('/datatofront', function (request, response) {
    var date = Date.now()
    var data = request.body;
    console.log(data);
    wss.broadcast(JSON.stringify(Object.assign(data, { time: moment.utc(date).format('hh:mm:ss') })));
    response.send(request.body);
})