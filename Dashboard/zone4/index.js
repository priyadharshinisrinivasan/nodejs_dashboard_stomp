﻿var zone = 1;

function zoneSelect(id) {
    zone = id;
    
   }
$(document).ready(function () { 
    document.getElementById("zone4").style.backgroundColor = '#66cdaa';
    var timeData = [],
        temperatureData = [],
        humidityData = [],
        sensorstatusData = [],
        batterystatData = [],
        fuelleveldata = [],
        oilpressuredata = [],
        pressurelevData = [],
        watertemperatureData = [],
        cctvstatusData = [],
        volt1Data = [],
        volt2Data = [],
        volt3Data = [],
        kwh1Data = [],
        kwh2Data = [],
        kwh3Data = [];

    var tempdata = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'Temperature',
                yAxisID: 'Temperature',
                borderColor: "#FF0000",
                pointBoarderColor: "#FF0000",
                backgroundColor: "#FF0000",
                pointHoverBackgroundColor: "#FF0000",
                pointHoverBorderColor: "#FF0000",
                data: temperatureData
            }
        ]
    }
    var humiddata = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'Humidity',
                yAxisID: 'Humidity',
                borderColor: "#FFFF66",
                pointBoarderColor: "#FFFF66",
                backgroundColor: "#FFFF66",
                pointHoverBackgroundColor: "#FFFF66",
                pointHoverBorderColor: "#FFFF66",
                data: humidityData
            }
        ]
    }
    var sensordata = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'SensorStatus',
                yAxisID: 'SensorStatus',
                borderColor: "#9ACD32",
                pointBoarderColor: "#9ACD32",
                backgroundColor: "#9ACD32",
                pointHoverBackgroundColor: "#9ACD32",
                pointHoverBorderColor: "#9ACD32",
                data: sensorstatusData
            }
        ]
    }
    var batterydata = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'BatteryStatus',
                yAxisID: 'BatteryStatus',
                borderColor: "#5F9EA0",
                pointBoarderColor: "#5F9EA0",
                backgroundColor: "#5F9EA0",
                pointHoverBackgroundColor: "#5F9EA0",
                pointHoverBorderColor: "#5F9EA0",
                data: batterystatData
            }
        ]
    }
    var fueldata = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'FuelLevel',
                yAxisID: 'FuelLevel',
                borderColor: "#4169E1",
                pointBoarderColor: "#4169E1",
                backgroundColor: "#4169E1",
                pointHoverBackgroundColor: "#4169E1",
                pointHoverBorderColor: "#4169E1",
                data: fuelleveldata
            }
        ]
    }
    var oildata = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'OilPressure',
                yAxisID: 'OilPressure',
                borderColor: "#A52A2A",
                pointBoarderColor: "#A52A2A",
                backgroundColor: "#A52A2A",
                pointHoverBackgroundColor: "#A52A2A",
                pointHoverBorderColor: "#A52A2A",
                data: oilpressuredata
            }
        ]
    }
    var pressureData = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'Pressure',
                yAxisID: 'Pressure',
                borderColor: "#3CB371",
                pointBoarderColor: "#3CB371",
                backgroundColor: "#3CB371",
                pointHoverBackgroundColor: "#3CB371",
                pointHoverBorderColor: "#3CB371",
                data: pressurelevData
            }
        ]
    }
    var watertempdata = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'Water Temperature',
                yAxisID: 'Water Temperature',
                borderColor: "#0000FF",
                pointBoarderColor: "#0000FF",
                backgroundColor: "#0000FF",
                pointHoverBackgroundColor: "#0000FF",
                pointHoverBorderColor: "#0000FF",
                data: watertemperatureData
            }
        ]
    }
    var cctvdata = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'CCTV Status',
                yAxisID: 'CCTV Status',
                borderColor: "#66cdaa",
                pointBoarderColor: "#66cdaa",
                backgroundColor: "#66cdaa",
                pointHoverBackgroundColor: "#66cdaa",
                pointHoverBorderColor: "#66cdaa",
                data: cctvstatusData
            }
        ]
    }
    var volt1 = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'Volt1 Data',
                yAxisID: 'Volt1 Data',
                borderColor: "#FF6347",
                pointBoarderColor: "#FF6347",
                backgroundColor: "#FF6347",
                pointHoverBackgroundColor: "#FF6347",
                pointHoverBorderColor: "#FF6347",
                data: volt1Data
            }
        ]
    }
    var volt2 = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'Volt2 Data',
                yAxisID: 'Volt2 Data',
                borderColor: "#FF6347",
                pointBoarderColor: "#FF6347",
                backgroundColor: "#FF6347",
                pointHoverBackgroundColor: "#FF6347",
                pointHoverBorderColor: "#FF6347",
                data: volt2Data
            }
        ]
    }
    var volt3 = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'Volt3 Data',
                yAxisID: 'Volt3 Data',
                borderColor: "#FF6347",
                pointBoarderColor: "#FF6347",
                backgroundColor: "#FF6347",
                pointHoverBackgroundColor: "#FF6347",
                pointHoverBorderColor: "#FF6347",
                data: volt3Data
            }
        ]
    }
    var kwh1value = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'kwh1 Data',
                yAxisID: 'kwh1 Data',
                borderColor: "#40E0D0",
                pointBoarderColor: "#40E0D0",
                backgroundColor: "#40E0D0",
                pointHoverBackgroundColor: "#40E0D0",
                pointHoverBorderColor: "#40E0D0",
                data: kwh1Data
            }
        ]
    }
    var kwh2value = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'kwh2 Data',
                yAxisID: 'kwh2 Data',
                borderColor: "#40E0D0",
                pointBoarderColor: "#40E0D0",
                backgroundColor: "#40E0D0",
                pointHoverBackgroundColor: "#40E0D0",
                pointHoverBorderColor: "#40E0D0",
                data: kwh2Data
            }
        ]
    }
    var kwh3value = {
        labels: timeData,
        datasets: [
            {
                fill: false,
                label: 'kwh3 Data',
                yAxisID: 'kwh3 Data',
                borderColor: "#40E0D0",
                pointBoarderColor: "#40E0D0",
                backgroundColor: "#40E0D0",
                pointHoverBackgroundColor: "#40E0D0",
                pointHoverBorderColor: "#40E0D0",
                data: kwh3Data
            }
        ]
    }
    var basicOption = {
        title: {
            display: true,
            text: 'Temperature Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'Temperature',
                type: 'linear',
                scaleLabel: {
                    labelString: 'Temperature(C)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)',

                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)',
                    fontSize: 10,
                },
                position: 'left',

            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },

            }]
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 255, 255)',

            },

        }
    }

    var basichumidOption = {
        title: {
            display: true,
            text: 'Humidity Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'Humidity',
                type: 'linear',
                scaleLabel: {
                    labelString: 'Humidity(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]

        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var basicsensorOption = {
        title: {
            display: true,
            text: 'Sensor Status Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'SensorStatus',
                type: 'linear',
                scaleLabel: {
                    labelString: 'SensorStatus(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var basicbatdataOption = {
        title: {
            display: true,
            text: 'Battery Status Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'BatteryStatus',
                type: 'linear',
                scaleLabel: {
                    labelString: 'BatteryStatus(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var basicfueldataOption = {
        title: {
            display: true,
            text: 'Fuel Level Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'FuelLevel',
                type: 'linear',
                scaleLabel: {
                    labelString: 'FuelLevel(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var basicoildataOption = {
        title: {
            display: true,
            text: 'Oil Pressure Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'OilPressure',
                type: 'linear',
                scaleLabel: {
                    labelString: 'OilPressure()',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var basicpressuredataOption = {
        title: {
            display: true,
            text: 'Pressure Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'Pressure',
                type: 'linear',
                scaleLabel: {
                    labelString: 'Pressure()',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var basicwatertempdataOption = {
        title: {
            display: true,
            text: 'Water Temperature Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'Water Temperature',
                type: 'linear',
                scaleLabel: {
                    labelString: 'Water Temperature(C)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,

            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var cctvstatusdataOption = {
        title: {
            display: true,
            text: 'CCTV Status Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'CCTV Status',
                type: 'linear',
                scaleLabel: {
                    labelString: 'CCTV Status(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,

            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var volt1leveldataOption = {
        title: {
            display: true,
            text: 'Volt 1 Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'Volt1 Data',
                type: 'linear',
                scaleLabel: {
                    labelString: 'Volt1(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,

            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var volt2leveldataOption = {
        title: {
            display: true,
            text: 'Volt 2 Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'Volt2 Data',
                type: 'linear',
                scaleLabel: {
                    labelString: 'Volt2(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,

            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var volt3leveldataOption = {
        title: {
            display: true,
            text: 'Volt 3 Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'Volt3 Data',
                type: 'linear',
                scaleLabel: {
                    labelString: 'Volt3(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,

            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var kWh1leveldataOption = {
        title: {
            display: true,
            text: 'kwh 1 Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'kwh1 Data',
                type: 'linear',
                scaleLabel: {
                    labelString: 'kwh1(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,

            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var kWh2leveldataOption = {
        title: {
            display: true,
            text: 'kwh 2 Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'kwh2 Data',
                type: 'linear',
                scaleLabel: {
                    labelString: 'kwh2(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,

            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }

    var kWh3leveldataOption = {
        title: {
            display: true,
            text: 'kwh 3 Real-time Data',
            fontSize: 15,
            fontColor: 'rgb(255, 255, 255)'
        },
        scales: {
            yAxes: [{
                id: 'kwh3 Data',
                type: 'linear',
                scaleLabel: {
                    labelString: 'kwh3(%)',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                gridLines: {
                    color: 'rgb(255, 255, 255)',
                    zeroLineColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
                position: 'left',
            }],
            xAxes: [{
                scaleLabel: {
                    labelString: 'Time',
                    display: true,
                    fontColor: 'rgb(255, 255, 255)'
                },
                ticks: {
                    fontColor: 'rgb(255, 255, 255)', fontSize: 10,
                },
            }]
        },
        legend: {
            display: true,

            labels: {
                fontColor: 'rgb(255, 255, 255)'
            }
        }
    }
    //Get the context of the canvas element we want to select
    //var ctxtemp = document.getElementById("tempChart").getContext("2d");
    //var optionsNoAnimation = { animation: false }
    var canvas = document.getElementById('tempChart');
    var context = canvas.getContext('2d'); 
  
    context.fillStyle = 'blue';
    var myLineChart = new Chart(context, {
        type: 'line',
        data: tempdata,
        options: basicOption,

    });

    var ctxhumid = document.getElementById("humidChart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var myhumidChart = new Chart(ctxhumid, {
        type: 'line',
        data: humiddata,
        options: basichumidOption
    });

    var ctxsensorstatus = document.getElementById("sensorstatuschart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var mysensorChart = new Chart(ctxsensorstatus, {
        type: 'line',
        data: sensordata,
        options: basicsensorOption
    });

    var ctxbatstatus = document.getElementById("batteryconditionChart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var mybatdataChart = new Chart(ctxbatstatus, {
        type: 'line',
        data: batterydata,
        options: basicbatdataOption,
       
    });

    var ctxfuellevel = document.getElementById("fuellevelChart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var myfuelLevelchart = new Chart(ctxfuellevel, {
        type: 'line',
        data: fueldata,
        options: basicfueldataOption
    });

    var ctxoilPressure = document.getElementById("oilpressureChart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var myoilPressurechart = new Chart(ctxoilPressure, {
        type: 'line',
        data: oildata,
        options: basicoildataOption
    });

    var ctxPressure = document.getElementById("pressureChart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var myPressurechart = new Chart(ctxPressure, {
        type: 'line',
        data: pressureData,
        options: basicpressuredataOption
    });

    var ctxWaterTemp = document.getElementById("watertemperaturechart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var myWaterTempchart = new Chart(ctxWaterTemp, {
        type: 'line',
        data: watertempdata,
        options: basicwatertempdataOption
    });

    var ctxstatusData = document.getElementById("cctvstatusChart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var mycctvstatuschart = new Chart(ctxstatusData, {
        type: 'line',
        data: cctvdata,
        options: cctvstatusdataOption
    });

    var ctxvolt1sData = document.getElementById("volt1chart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var myvolt1chart = new Chart(ctxvolt1sData, {
        type: 'line',
        data: volt1,
        options: volt1leveldataOption
    });

    var ctxvolt2sData = document.getElementById("volt2chart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var myvolt2chart = new Chart(ctxvolt2sData, {
        type: 'line',
        data: volt2,
        options: volt2leveldataOption
    });

    var ctxvolt3sData = document.getElementById("volt3chart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var myvolt3chart = new Chart(ctxvolt3sData, {
        type: 'line',
        data: volt3,
        options: volt3leveldataOption
    });

    var ctxkwh1Data = document.getElementById("kWh1chart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var mykwh1chart = new Chart(ctxkwh1Data, {
        type: 'line',
        data: kwh1value,
        options: kWh1leveldataOption
    });

    var ctxkwh2Data = document.getElementById("kWh2chart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var mykwh2chart = new Chart(ctxkwh2Data, {
        type: 'line',
        data: kwh2value,
        options: kWh2leveldataOption
    });

    var ctxkwh3Data = document.getElementById("kWh3chart").getContext("2d");
    var optionsNoAnimation = { animation: false }
    var mykwh3chart = new Chart(ctxkwh3Data, {
        type: 'line',
        data: kwh3value,
        options: kWh3leveldataOption
    });

    //var dps = []; // dataPoints
    //var chart = new CanvasJS.Chart("chartContainer", {
    //    title: {
    //        text: "Real time Temperature Data"
    //    },
    //    axisY: {
    //        title: "Temperature",
    //        includeZero: false
    //    },
    //    axisX: {
    //        title: "time",
    //        valueFormatString: "DD-MMM-YY hh-mm-ss",
    //        labelAutoFit: true
    //    },
    //    data: [{
    //        type: "line",
    //        dataPoints: dps,
    //        lineColor: "#66cdaa",
    //        markerColor: "#66cdaa",
    //    }]
    //});  
   

    //var updateChart = function (obj) {        
    //        dps.push({
    //            x: new Date(obj.time),
    //                //"2018:04: 23T09: 40:37"),
    //            y: parseInt("76")
    //        });          
           
    //    if (dps.length > obj.length) {
    //        dps.shift();
    //    }

    //    chart.render();
    //};

  

   
    var obj;
    var ws = new WebSocket('ws://'+location.hostname +':8087/client');
    ws.onopen = function () {
        console.log('Successfully connect WebSocket');
    }
    ws.onmessage = function (message) {
        var obj_recv = JSON.parse(message.data);
        console.log(obj_recv);
        obj = obj_recv;
        if (obj.content.Zone == 4) {
            if (obj.content.FireAlert == "1") {
                document.getElementById("firealert").src = "https://powerbivis.blob.core.windows.net/powerbivis/FireAlert.png";

            }
            else {
                document.getElementById("firealert").src = "https://powerbivis.blob.core.windows.net/powerbivis/Firenorm.png";

            }
            if (obj.content.SmokeAlert == "1") {
                document.getElementById("smokealert").src = "https://powerbivis.blob.core.windows.net/powerbivis/SmokeAlert.png";

            }
            else {
                document.getElementById("smokealert").src = "https://powerbivis.blob.core.windows.net/powerbivis/Smokenorm.png";

            }
            if (obj.content.Alarm == "1") {
                document.getElementById("alarm").src = "https://powerbivis.blob.core.windows.net/powerbivis/alarmalert.jpg";

            }
            else {
                document.getElementById("alarm").src = "https://powerbivis.blob.core.windows.net/powerbivis/alarmnorm.jpg";

            }
            if (obj.content.GasLeakageAlert == "1") {
                document.getElementById("gasleakagealert").src = "https://powerbivis.blob.core.windows.net/powerbivis/gasleakagealert.jpg";

            }
            else {
                document.getElementById("gasleakagealert").src = "https://powerbivis.blob.core.windows.net/powerbivis/gasleakagenorm.jpg";

            }
            if (obj.content.GeneralFault == "1") {
                document.getElementById("generalfault").src = "https://powerbivis.blob.core.windows.net/powerbivis/faultalert.jpg";

            }
            else {
                document.getElementById("generalfault").src = "https://powerbivis.blob.core.windows.net/powerbivis/faultnorm.jpg";

            }

            if (obj.content.FireDoor == "1") {
                document.getElementById("firedooralert").src = "https://powerbivis.blob.core.windows.net/powerbivis/firedooralert.png";

            }
            else {
                document.getElementById("firedooralert").src = "https://powerbivis.blob.core.windows.net/powerbivis/firedoornorm.png";

            }
            if (obj.content.Mains == "1") {
                document.getElementById("mainsalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/mainsalert.png";

            }
            else {
                document.getElementById("mainsalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/mainsnorm.png";

            }
            if (obj.content.Pump == "1") {
                document.getElementById("pumpalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/pumpalert.jpg";

            }
            else {
                document.getElementById("pumpalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/pumpnorm.jpg";

            }
            if (obj.content.Sprinkler == "1") {
                document.getElementById("sprinkleralert").src = "https://powerbivis.blob.core.windows.net/powerbivis/SpriklerAlert.jpg";

            }
            else {
                document.getElementById("sprinkleralert").src = "https://powerbivis.blob.core.windows.net/powerbivis/Spriklernorm.jpg";

            }
            if (obj.content.ManualTest == "1") {
                document.getElementById("manualtestalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/manualalert.png";

            }
            else {
                document.getElementById("manualtestalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/manualnorm.png";

            }
            if (obj.content.Auxiliary == "1") {
                document.getElementById("auxillaryalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/auxalert.png";

            }
            else {
                document.getElementById("auxillaryalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/auxnorm.png";

            }
            if (obj.content.EngineRunning == "1") {
                document.getElementById("enginerunningalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/enginealert.png";

            }
            else {
                document.getElementById("enginerunningalert").src = "https://powerbivis.blob.core.windows.net/powerbivis/enginenorm.png";

            }

            try {
                //var obj_1 = JSON.stringify(message.data);

                if (!obj.time || !obj.content.Temperature) {
                    return;
                }
                timeData.push(obj.time);
                temperatureData.push(obj.content.Temperature);

                //updateChart(obj);



                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    temperatureData.shift();
                }

                //if (obj.content.Humidity) {
                //    humidityData.push(obj.content.Humidity);
                //}
                //if (humidityData.length > maxLen) {
                //    humidityData.shift();
                //}

                myLineChart.update();
            } catch (err) {
                console.error(err);
            }

            try {
                //var obj_1 = JSON.stringify(message.data);

                if (!obj.time || !obj.content.Humidity) {
                    return;
                }
                //timeData.push(obj.time);
                humidityData.push(obj.content.Humidity);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    humidityData.shift();
                }

                //if (obj.content.Humidity) {
                //    humidityData.push(obj.content.Humidity);
                //}
                //if (humidityData.length > maxLen) {
                //    humidityData.shift();
                //}

                myhumidChart.update();
            } catch (err) {
                console.error(err);
            }
            try {
                //var obj_1 = JSON.stringify(message.data);

                if (!obj.time || !obj.content.SensorStatus) {
                    return;
                }
                //timeData.push(obj.time);
                sensorstatusData.push(obj.content.SensorStatus);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    sensorstatusDatas.shift();
                }

                //if (obj.content.Humidity) {
                //    humidityData.push(obj.content.Humidity);
                //}
                //if (humidityData.length > maxLen) {
                //    humidityData.shift();
                //}

                mysensorChart.update();
            } catch (err) {
                console.error(err);
            }

            try {
                //var obj_1 = JSON.stringify(message.data);

                if (!obj.time || !obj.content.BatteryCondition) {
                    return;
                }
                if (obj.content.BatteryCondition == "On") {
                    batterystatData.push("1");
                }
                //timeData.push(obj.time);
                else {
                    batterystatData.push("0");
                }

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    batterystatData.shift();
                }

                mybatdataChart.update();
            } catch (err) {
                console.error(err);
            }
            try {
                //var obj_1 = JSON.stringify(message.data);

                if (!obj.time || !obj.content.FuelLevel) {
                    return;
                }
                //timeData.push(obj.time);
                fuelleveldata.push(obj.content.FuelLevel);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    fuelleveldata.shift();
                }

                myfuelLevelchart.update();
            } catch (err) {
                console.error(err);
            }
            try {
                //var obj_1 = JSON.stringify(message.data);

                if (!obj.time || !obj.content.OilPressure) {
                    return;
                }
                //timeData.push(obj.time);
                oilpressuredata.push(obj.content.OilPressure);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    oilpressuredata.shift();
                }

                myoilPressurechart.update();
            } catch (err) {
                console.error(err);
            }
            try {
                //var obj_1 = JSON.stringify(message.data);

                if (!obj.time || !obj.content.Pressure) {
                    return;
                }
                //timeData.push(obj.time);
                pressurelevData.push(obj.content.Pressure);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    pressurelevData.shift();
                }

                myPressurechart.update();
            } catch (err) {
                console.error(err);
            }
            try {
                //var obj_1 = JSON.stringify(message.data);

                if (!obj.time || !obj.content.WaterTemperature) {
                    return;
                }
                //timeData.push(obj.time);
                watertemperatureData.push(obj.content.WaterTemperature);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    watertemperatureData.shift();
                }

                myWaterTempchart.update();
            } catch (err) {
                console.error(err);
            }
            try {
                //var obj_1 = JSON.stringify(message.data);

                if (!obj.time || !obj.content.CCTVStatus) {
                    return;
                }
                //timeData.push(obj.time);
                cctvstatusData.push(obj.content.CCTVStatus);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    cctvstatusData.shift();
                }

                mycctvstatuschart.update();
            } catch (err) {
                console.error(err);
            }
            try {

                if (!obj.time || !obj.content.Volt1) {
                    return;
                }
                //timeData.push(obj.time);
                volt1Data.push(obj.content.Volt1);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    volt1Data.shift();
                }

                myvolt1chart.update();
            } catch (err) {
                console.error(err);
            }
            try {

                if (!obj.time || !obj.content.Volt2) {
                    return;
                }
                //timeData.push(obj.time);
                volt2Data.push(obj.content.Volt2);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    volt2Data.shift();
                }

                myvolt2chart.update();
            } catch (err) {
                console.error(err);
            }
            try {

                if (!obj.time || !obj.content.Volt3) {
                    return;
                }
                //timeData.push(obj.time);
                volt3Data.push(obj.content.Volt3);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    volt3Data.shift();
                }

                myvolt3chart.update();
            } catch (err) {
                console.error(err);
            }
            try {

                if (!obj.time || !obj.content.KWH1) {
                    return;
                }
                //timeData.push(obj.time);
                kwh1Data.push(obj.content.KWH1);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    kwh1Data.shift();
                }

                mykwh1chart.update();
            } catch (err) {
                console.error(err);
            }
            try {

                if (!obj.time || !obj.content.KWH2) {
                    return;
                }
                //timeData.push(obj.time);
                kwh2Data.push(obj.content.KWH2);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    kwh2Data.shift();
                }

                mykwh2chart.update();
            } catch (err) {
                console.error(err);
            }
            try {

                if (!obj.time || !obj.content.KWH3) {
                    return;
                }
                //timeData.push(obj.time);
                kwh3Data.push(obj.content.KWH3);

                // only keep no more than 50 points in the line chart
                const maxLen = 10;
                var len = timeData.length;
                if (len > maxLen) {
                    timeData.shift();
                    kwh3Data.shift();
                }

                mykwh3chart.update();
            } catch (err) {
                console.error(err);
            }

        }
        else {
            console.log('No Data for zone 2');
        }


    }
})